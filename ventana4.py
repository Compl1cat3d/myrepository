from tkinter import *
from tkinter import ttk
from math import pow
from math import sin
from math import cos
from math import tan
from math import pi
from math import e
from math import sinh
from math import cosh
from math import tanh
from math import log10
from math import log
from math import sqrt
'''ta los cojones'''

x=1
funcion= "2*x+cos(x)"
lista =[]
colores = []
view_size = 20
COMPUTATION_DISTANCE = 0.001
primeraX = 0
primeraY = 0
xAnt = 0 
yAnt = 0
cont = 0
cord = []
cord1 = () 


def draw_grid():
    draw_line(view_size * -1, 0, view_size, 0, "darkgray")
    draw_line(0, view_size * -1, 0, view_size, "darkgray")



def draw_line(x_from, y_from, x_to, y_to, colour):
    from_coord = translate(x_from, y_from)
    to_coord = translate(x_to, y_to)
    
    liine = canvas.create_line(from_coord[0], from_coord[1], to_coord[0], to_coord[1], fill=colour)



def translate(x_current, y_current):
    tc = [0, 0]
    x_mul = int(canvas["width"]) / (view_size * 2)
    y_mul = (int(canvas["height"]) / (view_size * -2))
    x_current = (x_current + view_size) * x_mul
    y_current = (y_current + view_size) * y_mul + int(canvas["height"])
    tc[0] = x_current
    tc[1] = y_current
    return tc


def add():
    for i in range(0,len(lista)):
        if e.get() == lista[i]:
            return
        
    lista.append(e.get())
    colores.append(combo.get())
            
def draw_graph():
    
    add()
    funcion = e.get()
    color = combo.get()
    
    combo2["values"] = lista
    draw_grid()
    y_previous = 0.0
    x = view_size * -1
    while x < view_size:
        try:
            y = eval(funcion)
            
        except ValueError:
            y = 0
        try:
            draw_line(x -  COMPUTATION_DISTANCE *view_size, y_previous, x, y, color)
        except:
            
            break
        y_previous = y
        x += COMPUTATION_DISTANCE * view_size


def clear():
    global lista
    
    lista=[]
    
    combo2["values"]= []
    canvas.delete("all")
    draw_grid()



def clean_one():
    
    
    funcion = combo2.get()
    canvas.delete("all")   

    for i in range(0,len(lista)): 
            
        if funcion == lista[i]:
            lista.pop(i)
            colores.pop(i)
            combo2["values"] = lista
            break
            
    print(lista)      
  
    for i in range (0,len(lista)):
        print(i)
        y_previous = 0.0
        x = view_size * -1
            
        draw_grid()
        funcion = lista[i]
        print (funcion)
        while x <= view_size:
            try:
                y = eval(funcion)
                
            except ValueError:
                print("hola")
            try:
                draw_line(x -  COMPUTATION_DISTANCE *view_size, y_previous, x, y, colores[i])
            except:
                print ( f"Que esta pasando {x}")
            y_previous = y
            x += COMPUTATION_DISTANCE * view_size

def mouseDragged(event):
    global cont
    global xAnt 
    global yAnt
    

    x1 = event.x
    y1 = event.y
    if 1000 <= (xAnt - x1)**2 +(yAnt-y1)**2:
        if cont <= 8:        
            cont += 1
            xAnt = x1
            yAnt = y1
            cord1 = ()
            cord1=(x1,y1)
            cord.append(cord1)
            
            
    print (cord)
    
    

def onClick(event):
    global cont
    global primeraX
    global primeraY
    global xAnt
    global yAnt
    global cord
    cont = 0 
    cord = []
    primeraX = event.x
    primeraY = event.y
    xAnt = primeraX
    yAnt = primeraY
    

###########################################################

root = Tk()
root.wm_title("Calculator 3000")
root.resizable(width=False, height=False)
horizontal_screen = root.winfo_screenwidth() / 2 - root.winfo_reqwidth()
vertical_screen = root.winfo_screenheight() / 2 - root.winfo_reqheight()

###########################################################

e = Entry(root)
e.grid(row = 1, column = 0 , columnspan = 5 , sticky = W + E)
e.insert(END, '2*x+cos(x)')

########################################################### 
combo = ttk.Combobox(root, state="readonly")
combo.grid(row = 8, column=0 ,columnspan = 3)
combo["values"] = ["Black" , "Green" , "Yellow" , "Red", "Blue"]
combo.current(3)
combo2 = ttk.Combobox(root, state="readonly")
combo2.grid(row = 9, column=0 ,columnspan = 4 , sticky = W + E )


###########################################################

canvas = Canvas(root)
canvas.configure(bg = "white")
canvas.grid(row=0, column=0, columnspan=5)

###########################################################

btn_clear = ttk.Button(root, text="Clear", command=lambda: clear()).grid(row=8, column=4)
btn_enter = ttk.Button(root, text="Enter", command=lambda:draw_graph())
btn_clear_one = ttk.Button(root,text = ("-"), command =lambda: clean_one())

btn_enter.grid(row=8, column=3)
btn_clear_one.grid(row= 9 ,column = 4)

###########################################################

draw_grid()
draw_graph()
root.bind("<Button-1>",onClick)

root.bind("<B1-Motion>",mouseDragged)
root.mainloop()